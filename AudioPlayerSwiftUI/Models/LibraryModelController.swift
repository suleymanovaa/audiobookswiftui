//
//  LibraryModdel.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 29/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import SwiftUI
import CoreLocation

struct Library: Hashable, Codable, Identifiable  {
    var id: Int
    var name: String
    var url: String
    var login: String
    var password: String
    var path: String
    var selector: Int
    var playlistList: [Playlist]

}
