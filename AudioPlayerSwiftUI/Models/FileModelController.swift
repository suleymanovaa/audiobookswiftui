//
//  FileModelController.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 29/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import SwiftUI
import CoreLocation


struct File: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var progress: Double
    var path: String
}
