//
//  PlaylistModelController.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 29/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import Foundation
import SwiftUI
import CoreLocation


struct Playlist: Hashable, Codable, Identifiable   {
    var id: Int
    var name: String
    var selector: Int
    var fileList: [File]
    
    func play() {
    }
}

