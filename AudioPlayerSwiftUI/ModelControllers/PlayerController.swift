//
//  PlayerController.swift
//  AudioBookPlayer
//
//  Created by Сулейманов Алексей on 30/05/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import Foundation
import AVFoundation
import FilesProvider

protocol PropertyObserver{
    func didGet(selector: Int)
}

protocol Subject{
    func add(observer: PropertyObserver)
    func remove(observer: PropertyObserver)
    func notity(selector: Int)
}

protocol PlayerController {
    func play()
    func pause()
    func stop()
    func next()
    func previos()
    func rewind( to interval: Int )
}

class WebDAVPlayerController: NSObject {
    
    var observerCollection = NSMutableSet()
    
    var buffer: AVAudioBuffer = AVAudioBuffer()
    var index: Int64 = 0

    var modelController: ModelController?
    
    var player: AVAudioPlayer?
    var webDAV: WebDAVFileProvider?
    
    var playlist: Playlist? 
    var library: Library?
    
    init(modelConroller: ModelController) {
        super.init()
        self.modelController = modelConroller
        self.modelController?.selector = 0
        library = modelController?.selectedLibrary()
        library?.selector = 0
        playlist = library?.selectedPlaylist()
        playlist?.selector = 0
        connectToServer()
    }
    
    private func connectToServer() {
        guard
            let url = library?.url ,
            let login = library?.login,
            let password = library?.password
        else { return }
        
        let credential = URLCredential(user: login, password: password, persistence: .permanent)
        webDAV = WebDAVFileProvider(baseURL: URL(string: url)!, credential: credential)
        webDAV?.delegate = self as? FileProviderDelegate
    }
}

extension WebDAVPlayerController: PlayerController {
    
    func play() {
        guard let webDAV = webDAV else {
            print("webDAV is nil")
            return
        }
        
        guard let file = playlist?.fileProgress() else {
            print("file is nil")
            return
        }
        
        if player?.isPlaying == true {
            player?.stop()
        }
        
        let completionHandler:((Data?, Error?) -> Void)! = { [weak self]
            contents,error in
            if let contents = contents {
                DispatchQueue.main.async {
                    do {
                        self?.player = try AVAudioPlayer(data: contents)
                        self?.player?.delegate = self
                        self?.player?.play()
                    } catch {
                        print("Player: \( error )")
                    }
                }
            }
        }
        webDAV.contents(path: file.path!, completionHandler: completionHandler)
    }
    
    func pause() {
        player?.pause()
    }
    
    func stop() {
        player?.stop()
    }
    
    func next() {
        playlist?.nextFile()
        play()
    }
    
    func previos() {
        playlist?.previuosFile()
        play()
    }
    
    func rewind(to interval: Int) {
    }
}

extension WebDAVPlayerController : AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        next()
    }
}

