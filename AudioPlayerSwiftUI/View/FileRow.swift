//
//  FileRow.swift
//  AudioPlayerSwiftUI
//
//  Created by Сулейманов Алексей on 15.06.2019.
//  Copyright © 2019 alex. All rights reserved.
//

import SwiftUI

struct FileRow : View {
    
    var file: File
    
    var body: some View {
        HStack {
            Text(file.name)
        }
    }
}

#if DEBUG
struct FileRow_Previews : PreviewProvider {
    static var previews: some View {
        FileRow(file: libraryData[0].playlistList[0].fileList[0])
            .previewLayout(.fixed(width: 300, height: 70))
    }
}
#endif
