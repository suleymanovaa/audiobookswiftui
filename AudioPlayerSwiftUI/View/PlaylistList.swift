//
//  PlaylistList.swift
//  AudioPlayerSwiftUI
//
//  Created by Сулейманов Алексей on 15.06.2019.
//  Copyright © 2019 alex. All rights reserved.
//

import SwiftUI

struct PlaylistList : View {
    
    var library : Library
    
    var body: some View {
        NavigationView{
            List(library.playlistList) { playlist in
                NavigationButton(destination: PlayerView(playlist: playlist)){
                     PlaylistRow(playlist: playlist)
                }
            }
            .navigationBarTitle(Text("Playlists"))
        }
        
    }
}

#if DEBUG
struct PlaylistList_Previews : PreviewProvider {
    static var previews: some View {
        PlaylistList(library: libraryData[0])
    }
}
#endif
