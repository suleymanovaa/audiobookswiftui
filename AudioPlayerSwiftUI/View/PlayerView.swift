//
//  PlayerView.swift
//  AudioPlayerSwiftUI
//
//  Created by Сулейманов Алексей on 15.06.2019.
//  Copyright © 2019 alex. All rights reserved.
//

import SwiftUI

struct PlayerView : View {
    
    var playlist: Playlist
    
    var body: some View {
        
        NavigationView {
            VStack {
                Button(action: {
                    self.playlist.play()
                }) {
                    Text("Play")
                }
                List(playlist.fileList) { file in
                    FileRow(file: file)
                }
                
            }
            .navigationBarTitle(Text(playlist.name))
        }
    }
}

#if DEBUG
struct PlayerView_Previews : PreviewProvider {
    static var previews: some View {
        PlayerView(playlist: libraryData[0].playlistList[0])
    }
}
#endif
