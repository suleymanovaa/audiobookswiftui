
//
//  PLayList.swift
//  AudioPlayerSwiftUI
//
//  Created by Сулейманов Алексей on 15.06.2019.
//  Copyright © 2019 alex. All rights reserved.
//
import SwiftUI

struct PlaylistRow : View {
    
    var playlist: Playlist
    
    var body: some View {
        HStack {
            Text(playlist.name)
        }
    }
}

#if DEBUG
struct PlaylistRow_Previews : PreviewProvider {
    static var previews: some View {
        PlaylistRow(playlist: libraryData[0].playlistList[0])
            .previewLayout(.fixed(width: 300, height: 70))
    }
}
#endif
