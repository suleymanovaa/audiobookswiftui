//
//  LibraryRow.swift
//  AudioPlayerSwiftUI
//
//  Created by Сулейманов Алексей on 14.06.2019.
//  Copyright © 2019 alex. All rights reserved.
//

import SwiftUI

struct LibraryRow : View {
    
    var library: Library
    
    var body: some View {
        HStack {
            Text(library.name)
        }
    }
}

#if DEBUG
struct LibraryRow_Previews : PreviewProvider {
    static var previews: some View {
        LibraryRow(library: libraryData[0])
            .previewLayout(.fixed(width: 300, height: 70))
    }
}
#endif
