//
//  LibraryList.swift
//  AudioPlayerSwiftUI
//
//  Created by Сулейманов Алексей on 14.06.2019.
//  Copyright © 2019 alex. All rights reserved.
//

import SwiftUI

struct LibraryList : View {
    
    var body: some View {
        
        NavigationView {
            List(libraryData) { library in
                NavigationButton(destination: PlaylistList(library: library)){
                    LibraryRow(library: library)
                }
            }
            .navigationBarTitle(Text("Libraries"), displayMode: .large)
        }
    }
}
#if DEBUG
struct LibraryList_Previews : PreviewProvider {
    static var previews: some View {
        LibraryList()
    }
}
#endif
